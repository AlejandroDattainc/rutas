import { Component, OnInit } from '@angular/core';


// modelos de los datos
import { User } from '../../models/user';

//import servicios
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css'],
})
export class UserViewComponent implements OnInit {

  users: User[] = [];
  usersFilter: User[] = []

  constructor(private usersservice: UsersService) { }

  ngOnInit(): void {
    this.usersFilter = this.users = this.usersservice.getUsers();
  }

  checarFiltro(event: string){
      this.usersFilter = this.users.filter(user => user.rfc.toLowerCase().indexOf(event) > -1)

      // this.usersFilter = this.users.filter(user => {
      //   return user.rfc.indexOf(event) > -1
      // })
  }
}
