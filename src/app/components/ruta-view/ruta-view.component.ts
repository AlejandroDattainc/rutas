import { Component, OnInit } from '@angular/core';

// la importacion de las dependencias
import { RutasService } from '../../services/rutas.service';
import { AlertsService } from '../../services/alerts.service';

// modelos importados
import { Rutas } from '../../models/ruta';
import { RutaResponse } from '../../models/rutaResponse';

@Component({
  selector: 'app-ruta-view',
  templateUrl: './ruta-view.component.html',
  styleUrls: ['./ruta-view.component.css']
})
export class RutaViewComponent implements OnInit {


  rutas: Rutas[] = [];
  ruta: Rutas = {
    id: '',
    descripcion: ''
  }
  response: RutaResponse[] = [];
  habEdit: Boolean = false;
  habVal: Boolean = false;

  constructor(private rutasservice: RutasService, private as: AlertsService) { }

  ngOnInit() {
    this.getRutas();
  }

  getRutas() {
    this.rutasservice.getAllRutas().subscribe({
      next: data => {
        this.rutas = data;
      },
      error: error => {
        this.as.alertError(error);
      }
    })
  }

  setRuta() {
    if (this.validarInputs()) {
      this.rutasservice.setRuta(this.ruta).subscribe(data => {
        this.as.alertTimer(data[0]);
        this.getRutas();
      })
    }
  }

  deleteRuta(id: string) {
    this.rutasservice.deleteRuta(id).subscribe({
      next: data => {   
          this.as.alertTimer(data[0])
          this.getRutas();

      },error: error => {
          this.as.alertError(error);
        }
      })
  }

  editRuta() {
    this.rutasservice.editRuta(this.ruta).subscribe(data => {
      this.as.alertTimer(data[0]);
      this.habEdit = false;
      this.getRutas();
      this.ruta = {
        id: '',
        descripcion: ''
      }
    })
  }

  habEdicion(ruta: Rutas) {
    this.ruta = JSON.parse(JSON.stringify(ruta));
    this.habEdit = true;
    this.habVal = false;
  }

  canEdicion() {
    this.habEdit = false;
    this.ruta = {
      id: '',
      descripcion: ''
    }
  }

  validarInputs() {
    if (this.ruta.id != '' && this.ruta.descripcion != '') {
      this.habVal = false;
      return true;
    } else {
      this.habVal = true;
      return false;

    }
  }


}
