import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// importacion de servicios
import { UsersService } from '../../services/users.service';

// importacion de modelos
import { User } from '../../models/user';

@Component({
  selector: 'app-perfil-view',
  templateUrl: './perfil-view.component.html',
  styleUrls: ['./perfil-view.component.css']
})
export class PerfilViewComponent implements OnInit {

  id: number;
  user: User;
  constructor(private activatedroute: ActivatedRoute, private usersservice: UsersService) { }

  ngOnInit(): void {
    console.log(this.activatedroute.snapshot.params.id);
    this.getUser(this.activatedroute.snapshot.params.id);
  }

  getUser(id: number){
    this.user = this.usersservice.getUser(id);
  }

}
