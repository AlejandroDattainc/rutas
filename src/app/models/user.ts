export interface User{
    id: number,
    nombre: string,
    img: string,
    descripcion: string,
    correo: string,
    rfc: string,
}
