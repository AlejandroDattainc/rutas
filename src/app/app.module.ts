import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// importacion de servios
import { AlertsService } from './services/alerts.service';
import { RutasService } from './services/rutas.service';
import { UsersService } from './services/users.service';

// importacion de componentes principales
import { UserViewComponent } from './components/user-view/user-view.component';
import { RutaViewComponent } from './components/ruta-view/ruta-view.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PerfilViewComponent } from './components/perfil-view/perfil-view.component';


@NgModule({
  declarations: [
    AppComponent,
    UserViewComponent,
    RutaViewComponent,
    NotFoundComponent,
    PerfilViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [RutasService, AlertsService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
