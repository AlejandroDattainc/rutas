import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

// varibales del entorno
import { environment as env } from '../../environments/environment';

// modelos de datos
import { Rutas } from '../models/ruta';
import { RutaResponse } from '../models/rutaResponse';

@Injectable({
    providedIn: 'root'
})

export class RutasService {

    private urlRutas = env.urlRutas;

    constructor(private http: HttpClient) { }

    getAllRutas(): Observable<Rutas[]> {
        return this.http.get<Rutas[]>(this.urlRutas);
    }

    setRuta(ruta: Rutas): Observable<Rutas> {
        return this.http.post<Rutas>(this.urlRutas, ruta);
    }

    deleteRuta(id: string): Observable<RutaResponse[]> {
        return this.http.delete<RutaResponse[]>(this.urlRutas +"/"+ id);
    }

    editRuta(ruta: Rutas): Observable<RutaResponse[]>{
        return this.http.put<RutaResponse[]>(this.urlRutas, ruta);
    }

}
