import { Injectable } from '@angular/core';

// importacion de los modelos
import { User } from '../models/user';

@Injectable({
    providedIn: 'root'
})

export class UsersService {

    users: User[] = [];

    constructor() {

        this.users = [
            {
                id: 1,
                nombre: 'alex',
                img: null,
                descripcion: 'loremloremlorem lorem lorem loremlorem loremloremlorem lorem lorem loremloremloremloremlorem lorem lorem loremlorem',
                correo: 'alex@dattainc.com.mx',
                rfc: 'alex123456789'
            },
            {
                id: 2,
                nombre: 'alejandro',
                img: 'https://www.ivan-imperial.org/images/avatar/img.jpg',
                descripcion: 'loremloremlorem lorem lorem loremlorem loremloremlorem lorem lorem loremloremloremloremlorem lorem lorem loremlorem',
                correo: 'Alejandro@dattainc.com.mx',
                rfc: 'Alejandro123456789'
            },
            {
                id: 3,
                nombre: 'developer',
                img: null,
                descripcion: 'loremloremlorem lorem lorem loremlorem loremloremlorem lorem lorem loremloremloremloremlorem lorem lorem loremlorem',
                correo: 'developer@dattainc.com.mx',
                rfc: 'developer123456789'
            }]
    }

    getUsers(){
        return this.users;
    }

    getUser(id: number){
        for (let i = 0; i < this.users.length; i++) {
            if (this.users[i].id == id) {
                return this.users[i];
            }
        }
    }
}

// id: number,
// nombre: string,
// img: string,
// descripcion: string,
// correo: string,
// rfc: string,