import { Injectable } from '@angular/core';

// ES6 Modules or TypeScript
import Swal from 'sweetalert2'

// modelos
import { RutaResponse } from '../models/rutaResponse';

@Injectable({
    providedIn: 'root'
})

export class AlertsService {

    constructor() { }

    alertError(error) {
        Swal.fire({
            title: 'Upss!',
            text: 'ha ocurrido un error ' + error.message,
            icon: 'error',
            confirmButtonText: 'Aceptar'
        })
    }

    alertSuccess(msg: any) {
        Swal.fire({
            title: 'Correcto!!!',
            text: msg,
            icon: 'success',
            confirmButtonText: 'Aceptar'
        })
    }

    alertTimer(msg: RutaResponse) {
        if (msg.Resultado.split(':')[0] != 'Error') {
            Swal.fire({
                icon: 'success',
                title: msg.Resultado,
                showConfirmButton: false,
                timer: 1500
            })
        } else {
            Swal.fire({
                icon: 'error',
                title: msg.Resultado,
                showConfirmButton: false,
                timer: 2000
            })
        }
    }
}