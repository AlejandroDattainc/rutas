import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// componentes princpales de contenido
import { RutaViewComponent } from './components/ruta-view/ruta-view.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PerfilViewComponent } from './components/perfil-view/perfil-view.component';


const routes: Routes = [
  { path: '', component: RutaViewComponent },
  { path: 'rutas', component: RutaViewComponent },
  { path: 'user', component: UserViewComponent },
  { path: 'user/profile/:id', component: PerfilViewComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
